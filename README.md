[![Standard Version](https://img.shields.io/badge/release-standard%20version-brightgreen.svg?style=plastic)](https://github.com/conventional-changelog/standard-version)
[![License](https://img.shields.io/badge/license-MIT-green.svg?style=flat)](https://bitbucket.org/voodoo-crew/earthquakes-maps/blob/master/LICENSE)
[![dependencies Status](https://david-dm.org/voodoo-crew/earthquakes-maps/status.svg)](https://david-dm.org/voodoo-crew/earthquakes-maps)

# EarthQuakes Analizer and warnings Predictor with set of unique data visualizations #

`earthquakes` `google-maps` `heatmap`

---

## Live Demo ##
:point_right: See it in action at [EarthQuakes Center](http://bit.ly/eqm-gsm) :point_left:

---

## Articles ##

 - [ ] Google Maps
   - [ ] [Polygon](https://developers.google.com/maps/documentation/javascript/reference/polygon)
   - [ ] [SphericalGeometry ](https://developers.google.com/maps/documentation/javascript/geometry#SphericalGeometry) namespace
   - [ ] [Information Window](https://developers.google.com/maps/documentation/javascript/reference/info-window)

---

> Developed in **October 2018**

:scorpius:
